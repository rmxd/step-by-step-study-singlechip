#include <reg52.h>


/*

测试一： 注释掉第一行后报错========================

Rebuild started: Project: project
Rebuild target 'Target 1'
compiling main.c...
main.c(12): error C202: 'P2': undefined identifier
main.c(17): error C202: 'P2': undefined identifier
Target not created.
Build Time Elapsed:  00:00:01

报错原因分析：====================================

这是因为 主程序的P2没有定义 

探索：==============================================

通过everything 我们找找reg52.h 看看 P2 是怎样定义的

sfr P2    = 0xA0;  //P2 指向了0xA0 这个地址的寄存器

再探：============================================
观察内存 P2 地址

输入D:0xA0
添加断点 看P2值变化

测试二：
逻辑分析仪 观察一个引脚的脉冲变化 自行计算周期和频率


*/



void main()
{
	while(1)
	{

		int i=0;
		for(i=0;i<100;i++)
		{
			
			P2=0;
		  //P3=1; 为了加断点
		}
		
				for(i=0;i<100;i++)
		{
		P2=0xFF;
		//	P0=0; 为了加断点看
		}
		
		
	}		
}


// 这是注释代码  
/* 这是块注释
*
*
*/