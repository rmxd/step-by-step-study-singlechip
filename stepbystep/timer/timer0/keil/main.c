/*------------------------------------------------------------------*/
/* --- STC MCU Limited ---------------------------------------------*/
/* --- STC89-90xx Series 16-bit Timer Demo -------------------------*/
/* --- Mobile: (86)13922805190 -------------------------------------*/
/* --- Fax: 86-0513-55012956,55012947,55012969 ---------------------*/
/* --- Tel: 86-0513-55012928,55012929,55012966----------------------*/
/* --- Web: www.STCMCU.com -----------------------------------------*/
/* --- Web: www.GXWMCU.com -----------------------------------------*/
/* If you want to use the program or the program referenced in the  */
/* article, please specify in which data and procedures from STC    */
/*------------------------------------------------------------------*/

#include "reg51.h"	//引用头文件

//以下两个类型转换语句只是为了用短单词替代两个单词，写程序时方便
typedef unsigned char BYTE;	//BYTE 代替 unsinged char 代表一个字节长度
typedef unsigned int WORD;	//WORD 代替 unsigned int  代表两个字节（一个字）长度


//-----------------------------------------------

/* define constants */
#define FOSC 11059200L	//主晶振宏定义

#define T1MS (65536-FOSC/12/1000)	//1ms timer calculation method in 12T mode //宏定义 1ms定时器在12T模式下的计算方法

/* define SFR */
sbit TEST_LED = P1^0;	//work LED, flash once per second //工作LED 1秒闪烁1次

/* define variables */
WORD count;	//1000 times counter //1000毫秒计数器

unsigned char runCode;

//-----------------------------------------------

/* Timer0 interrupt routine */

void tm0_isr() interrupt 1
{
    runCode=5;

    TL0 = T1MS;	//reload timer0 low byte //重新装入定时器0低字节
    TH0 = T1MS >> 8;	//reload timer0 high byte //重新装入定时器0高字节
    if (count-- == 0)	//1ms * 1000 -> 1s //1ms定时计数1000次 后是1秒
    {
        runCode=6;
        count = 1000;	//reset counter //复位计数器
        TEST_LED = ! TEST_LED;	//work LED flash //LED灯闪烁
    }
}


void init_timer0()
{
    runCode=3;

    TMOD = 0x01;	//set timer0 as mode1 (16-bit) //设置定时器0 16bit工作模式
    TL0 = T1MS;	//initial timer0 low byte //初始化定时器0 低字节
    TH0 = T1MS >> 8;	//initial timer0 high byte //初始化定时器0 高字节
    TR0 = 1;	//timer0 start running //启动定时器0
    ET0 = 1;	//enable timer0 interrupt //开启定时器0中断
    EA = 1;	//open global interrupt switch //打开所有中断
    count = 0;	//initial counter //初始化计数器
}

/* main program 主程序*/
void main()
{
    runCode=1;
	
    init_timer0();	//初始化定时器函数


    while (1)	//主循环
    {
        runCode=2;
        ;	//分号代表空转，不做任何事情，但是；也会占用单片机运行时间
    }	//loop
}





